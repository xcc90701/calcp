//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    num1:'',
    is_2:0,
    num2:'',
    op:'',
    is_res:0,
    dp:'0'
  },
  //事件处理函数
  //用is_2来检查是不是第二个数字，如果是0，则赋值到num1，如果是1，则赋值到num2
  
  gainNum:function(e){
    var num;
    num = e.currentTarget.dataset.num;
    // console.log(temp);
    // nums = this.data.num1;
    // console.log(nums);
    // nums = nums + temp;
    // console.log(nums);
    if(this.data.is_2==0){
      this.clear();
      this.setData({
        dp:''
      });
      this.setData({
        num1: this.data.num1 + num,
        dp: this.data.dp + num
      });
      // console.log("num1: "+this.data.num1);      
    }else{
      this.setData({
        num2: this.data.num2 + num,
        dp: this.data.dp + num
      });
      // console.log("num2: "+this.data.num2);
    }
    
    // console.log(this.data.num1);
    // console.log(e.currentTarget.dataset.num);
    
  },
  //运算符收集
  //多次运算：检测num2是否为空字符，如果是，则进行第一次运算，如果num2有其他字符，则将上一次运算结果保存到num1，num2赋值新的字符
  gainOP: function (e) {
    var op = e.currentTarget.dataset.op;
    if(this.data.num2!=""){
      // console.log("之前的num2: "+this.data.num2);
      this.getResult();
      this.setData({
        is_2: 1,
        num1:this.data.dp,
        num2:'',
        dp: this.data.dp + op,
        op: op
      });
      // console.log("num1: " + this.data.num1);
      // console.log("num2: "+this.data.num2);
    }else{
      this.setData({
        is_2: 1,
        dp: this.data.dp + op,
        op: op
      });
    }
    // console.log(this.data.op);
  },
  getResult:function(){
    var num1 = parseFloat(this.data.num1);
    var num2 = parseFloat(this.data.num2);
    var res=0;
    // console.log("getResult");
    switch (this.data.op){
      case "/":
        res = num1/num2;
        this.setData({
          dp:res,
          is_2:0
        });
        break;
      case "*":
        res = num1 * num2;
        this.setData({
          dp: res,
          is_2: 0
        });
        break;
      case "-":
        res = num1 - num2;
        this.setData({
          dp: res,
          is_2: 0
        });
        break;
      case "+":
        res = num1 + num2;
        this.setData({
          dp: res,
          is_2: 0
        });
        break;
    }
  },
  clear:function(){
    this.setData({
      num1:"",
      num2:"",
      dp:"0",
      op:"",
      is_2:0
    });
  }
})
